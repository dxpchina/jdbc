package com.d.demo1;

/**
 * @author carzy.
 * @date 16:37 2018/12/21
 */
public class MyDriver implements MySqlDriver {

    static {
        Manger.registerDriver(new MyDriver());
    }

    @Override
    public String print() {
        return "自定义驱动";
    }

}
