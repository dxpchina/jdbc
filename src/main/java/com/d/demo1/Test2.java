package com.d.demo1;

/**
 * 加载
 * 链接(验证,准备,解析)
 * 初始化
 *
 * @author carzy.
 * @date 16:37 2018/12/21
 */
public class Test2 {

    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException {

        ClassLoader loader = ClassLoader.getSystemClassLoader();

        Class clazz = loader.loadClass("com.d.demo1.MyDriver");

//        clazz.newInstance();

        String msg = Manger.getDrive().print();

        System.out.println(msg);

    }
}
