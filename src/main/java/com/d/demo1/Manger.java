package com.d.demo1;

/**
 * @author carzy.
 * @date 16:29 2018/12/21
 */
public class Manger {

    private static MySqlDriver mySqlDriver;

    private Manger() {
    }

    public static void registerDriver(MySqlDriver driver) {
        mySqlDriver = driver;
    }

    public static MySqlDriver getDrive() {
        return mySqlDriver;
    }
}
