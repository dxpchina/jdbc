package com.dxp.demo3.mybatis;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * @author carzy
 * @date 2018/11/13
 */
public interface UserMapper {

    /**
     * 根据id 查找具体用户
     *
     * @param id 用户ID
     * @return 用户
     */
    User findOne(int id);

    /**
     * 查找所有用户
     *
     * @return 用户集
     */
    @Select("SELECT id, username, password, age FROM sys_user")
    List<User> findAll();

    /**
     * update user name
     *
     * @param username username
     * @param id       主键
     * @return 影响行数
     */
    @Update("update sys_user set username = #{username} where id = #{id}")
    int update(@Param("username") String username, @Param("id") int id);
}
