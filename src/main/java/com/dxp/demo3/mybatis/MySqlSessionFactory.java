package com.dxp.demo3.mybatis;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author carzy
 * @date 2018/11/13
 */
public class MySqlSessionFactory {

    private static SqlSessionFactory sqlSessionFactory;

    public static void init() {
        InputStream inputStream = null;
        if (sqlSessionFactory == null) {
            synchronized (MySqlSessionFactory.class) {
                if (sqlSessionFactory == null) {
                    try {
                        inputStream = Resources.getResourceAsStream("mybatis-config.xml");
                        sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
                        inputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public static SqlSession session() {
        return sqlSessionFactory.openSession();
    }

    public static SqlSession session(boolean f) {
        return sqlSessionFactory.openSession(f);
    }

}
