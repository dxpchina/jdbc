package com.dxp.demo3.mybatis;

import org.apache.ibatis.session.SqlSession;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author carzy
 * @date 2018/11/12
 */
@WebServlet("/db/test3")
public class DruidTestServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        SqlSession session = MySqlSessionFactory.session(true);
        UserMapper userMapper = session.getMapper(UserMapper.class);
        int user = userMapper.update("bbbbb", 1);
        session.close();
        PrintWriter os = resp.getWriter();
        os.write(String.valueOf(user));
        os.flush();
        os.close();
    }
}
