package com.dxp.demo3.mybatis;

import com.alibaba.druid.support.http.StatViewServlet;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;

/**
 * @author carzy
 * @date 2018/09/28
 */

@WebServlet(urlPatterns = {"/druid/*"}, initParams = {
        @WebInitParam(name = "resetEnable", value = "false"),
        @WebInitParam(name = "loginUsername", value = "admin"),
        @WebInitParam(name = "loginPassword", value = "tonmx")
})
public class DruidServlet extends StatViewServlet {
}
