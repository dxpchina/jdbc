package com.dxp.demo3.mybatis;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * @author carzy
 * @date 2018/09/18
 */
@WebListener
public class Init implements ServletContextListener {


    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        MySqlSessionFactory.init();
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
    }

}
