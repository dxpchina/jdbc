package com.dxp.demo4;

import com.sun.scenario.Settings;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.internal.SessionImpl;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.util.Map;

/**
 * @author carzy
 * @date 2018/11/14
 */
public class MainApp {

    public static void main(String[] args) throws Exception {
        Transaction transaction = null;
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

        try(Session session = sessionFactory.openSession()) {

//            System.out.println(((SessionImpl)session).connection());
            transaction = session.beginTransaction();

            User person = new User();
            person.setUsername("h_name_3");
            person.setPassword("h_pwd_3");
            person.setAge(1);
            session.save(person);

            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
        }
        HibernateUtil.shutdown();
    }

}
