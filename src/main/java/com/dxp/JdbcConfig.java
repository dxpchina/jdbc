package com.dxp;

/**
 * @author carzy.
 * @date 17:13 2018/12/21
 */
public interface JdbcConfig {

    String DRIVER_CLASS_NAME = "com.mysql.cj.jdbc.Driver";

    String URL = "jdbc:mysql://127.0.0.1:3306/test1?characterEncoding=utf8&useUnicode=true&serverTimezone=GMT%2b8";

    String USERNAME = "root";

    String PASSWORD = "";

}
