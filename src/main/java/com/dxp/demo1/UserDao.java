package com.dxp.demo1;

import com.dxp.JdbcConfig;

import java.sql.*;

/**
 * @author carzy
 * @date 2018/11/09
 */
public class UserDao {

    private static final String DRIVER_CLASS_NAME = JdbcConfig.DRIVER_CLASS_NAME;
    private static final String URL = JdbcConfig.URL;
    private static final String USERNAME = JdbcConfig.USERNAME;
    private static final String PASSWORD = JdbcConfig.PASSWORD;

    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        // 加载驱动
        Class.forName(DRIVER_CLASS_NAME);

        // 建立连接
        Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);

        // 操作DB
        String sql = "SELECT id, username, password, age FROM sys_user";
        PreparedStatement ps = connection.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            System.out.println(rs.getInt(1));
            System.out.println(rs.getString(2));
            System.out.println(rs.getString(3));
            System.out.println(rs.getInt(4));
        }

        // 关闭资源
        rs.close();
        ps.close();
        connection.close();

    }
}
