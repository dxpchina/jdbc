package com.dxp.demo1.base;

import com.dxp.JdbcConfig;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author carzy
 * @date 2018/11/09
 */
public class BaseDao {

    private static final String DRIVER_CLASS_NAME = JdbcConfig.DRIVER_CLASS_NAME;
    private static final String URL = JdbcConfig.URL;
    private static final String USERNAME = JdbcConfig.USERNAME;
    private static final String PASSWORD = JdbcConfig.PASSWORD;

    static {
        try {
            Class.forName(DRIVER_CLASS_NAME);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    Connection connection() {
        try {
            return DriverManager.getConnection(URL, USERNAME, PASSWORD);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
