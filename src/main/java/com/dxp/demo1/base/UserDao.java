package com.dxp.demo1.base;

import java.sql.*;

/**
 * @author carzy
 * @date 2018/11/09
 */
public class UserDao extends BaseDao {

    private void findAll() {
        String sql = "SELECT id, username, password, age FROM sys_user";
        try (PreparedStatement ps = this.connection().prepareStatement(sql); ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                System.out.println("id:" + rs.getInt(1));
                System.out.println("username:" + rs.getString(2));
                System.out.println("password:" + rs.getString(3));
                System.out.println("age:" + rs.getInt(4));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
        UserDao userDao = new UserDao();
        userDao.findAll();
    }
}
