package com.dxp.demo2.druid;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

/**
 * @author carzy
 * @date 2018/11/12
 */
//@WebServlet("/db/test")
public class DruidTestServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserDao userDao = UserDao.INSTANCE;
        PrintWriter os = resp.getWriter();
        StringBuilder builder = new StringBuilder();
        List<Map<String, Object>> list = userDao.findAll();
        list.forEach(m -> builder.append(m.toString()));
        System.out.println(builder);
        os.write(builder.toString());
        os.flush();
        os.close();
    }
}
