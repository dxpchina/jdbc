package com.dxp.demo2.druid;

import com.alibaba.druid.support.http.WebStatFilter;

import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;

/**
 * @author carzy
 * @date 2018/09/28
 */
//@WebFilter(urlPatterns = "/*", initParams = {
//        @WebInitParam(name = "exclusions", value = "/public/*,*.js,*.css,/druid*,*.jsp,*.swf"),
//        @WebInitParam(name = "principalSessionName", value = "sessionInfo"),
//        @WebInitParam(name = "profileEnable", value = "true"),
//})
public class DruidFilter extends WebStatFilter {
}
