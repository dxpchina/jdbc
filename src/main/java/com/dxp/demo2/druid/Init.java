package com.dxp.demo2.druid;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * @author carzy
 * @date 2018/09/18
 */
//@WebListener
public class Init implements ServletContextListener {


    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        DataPool.getInstance().stop();
    }

}
