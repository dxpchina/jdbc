package com.dxp.demo2.hikaircp;

/**
 * @author carzy
 * @date 2018/11/12
 */
public class HikaircpTest {

    public static void main(String[] args) {
        UserDao.INSTANCE.findAll().forEach(System.out::println);
        DataConfig.getInstance().close();
    }
}
