package com.dxp.demo2;

import java.util.stream.IntStream;

/**
 * @author carzy
 * @date 2018/11/12
 */
public class JdbcTest {

    public static void main(String[] args) {
        IntStream.range(0, 11).forEach(i -> {
            System.out.println("第" + (i + 1) + "次");
            UserDao.INSTANCE.findAll();
        });
    }
}
