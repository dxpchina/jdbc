package com.dxp.demo2;

import com.dxp.demo2.hikaircp.DataConfig;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author carzy
 * @date 2018/11/09
 */
public class UserDao {

    public static final UserDao INSTANCE = new UserDao();

    private UserDao() {
    }

    public List<Map<String, Object>> findAll() {
        String sql = "SELECT id, username, password, age FROM sys_user";
        List<Map<String, Object>> list = new ArrayList<>();
        Connection connection = DataConfig.getInstance().getConnection();
        System.out.println("connection" + connection);
        try (
                PreparedStatement ps = connection.prepareStatement(sql);
                ResultSet rs = ps.executeQuery()) {
            Map<String, Object> map;
            while (rs.next()) {
                map = new HashMap<>(4);
                map.put("id", rs.getInt("id"));
                map.put("username", rs.getString("username"));
                map.put("password", rs.getString("password"));
                map.put("age", rs.getInt("age"));
                list.add(map);
            }
        } catch (Exception ignored) {
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }
}
